FROM ubuntu:14.04
MAINTAINER Roni Choudhury <roni.choudhury@kitware.com>

EXPOSE 8080

# Make a package source for Mongo 3.
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
RUN echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" >/etc/apt/sources.list.d/mongodb-org-3.2.list

RUN apt-get update

RUN apt-get install -y \
    python \
    python-pip \
    mongodb-org \
    git \
    wget

RUN pip install \
    pymongo \
    tangelo

# Create a directory for Mongo to use.
RUN mkdir -p /data/db

# Create a tangelo user
RUN useradd -c "tangelo user" -m -d /home/tangelo -s /bin/bash tangelo

# Clone the repo.
RUN git clone https://gitlab.kitware.com/ronichoudhury/ner.git

# Change ownership of directory to tangelo.
RUN chown -R tangelo:tangelo ner

# Switch to the ner directory.
WORKDIR ner

# Download and unpack the data.
RUN wget https://data.kitware.com/api/v1/file/5697d3d38d777f429eac910d/download -O nercache.json.bz2
RUN bunzip2 nercache.json.bz2

# Set up some demodock vars.
ENV DEMODOCK_KEY ner
ENV DEMODOCK_READY TRUE

# Start the show.
COPY run.sh /ner/
CMD ["sh", "run.sh"]
